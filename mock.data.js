export const sites = [
  {
    _id: "1",
    name: "site 111",
    length: 2,
    address: "Hurshat Tal, Northern, Israel",
    description: "lorem ipsum",
    image: "lorem ipsum",
    visitorsPerMonth: 122,
    hasFoodArea: true,
    ticketPrice: 123,
    region: "Central"
  },
  {
    _id: "2",
    name: "site 2",
    length: 2,
    address: "Hurshat Tal, Northern, Israel",
    description: "lorem ipsum",
    image: "lorem ipsum",
    visitorsPerMonth: 0,
    hasFoodArea: false,
    ticketPrice: 150,
    region: "South"
  }
];

export const routes = [
  {
    _id: "123123",
    site: "1",
    name: "route 1",
    difficulty: "Intermediate",
    length: 12,
    description: "lalalalalalalal",
    image: "String",
    containsWater: true,
    recommendedSeason: "Summer"
  },
  {
    _id: "12312233",
    site: "1",
    name: "route 2",
    difficulty: "Intermediate",
    length: 12,
    description: "lalalalalalalal",
    image: "String",
    containsWater: true,
    recommendedSeason: "Spring"
  }
];
