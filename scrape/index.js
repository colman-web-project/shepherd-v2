const $ = require('cheerio');
const url = "https://www.s-capetravel.eu/search?iso=&durationMin=&durationMax=&theme_id%5B%5D=&level_id=&month=";
//const url = 'https://www.s-capetravel.eu/search?iso=FR&durationMin=&durationMax=&theme_id%5B%5D=11&level_id=&month=';
const codeToName = {
    "be": "Belgium",
    "hr": "Croatia",
    "fr": "France",
    "de": "Germany",
    "gr": "Greece",
    "hu": "Hungary",
    "it": "Italy",
    "nl": "Netherlands",
    "pt": "Spain",
    "sk": "Portugal",
    "es": "Slovakia",
    "se": "Sweden",
    "ch": "Switzerland",
    "gb": "United Kingdom"
}

const axios = require('axios');

const SCRAPE_IMAGE_START_WITH = "scrape_";

function scrape() {
    console.log("start")

    return axios.get(url).then(function (res) {
        const html = res.data;

        const allSites = $(".teaser", html);

        return Array
            .from(allSites)
            .filter(elem => $(".descriptionText > h5", elem).length != 0)
            .map(elem => {
                const header = $(".descriptionText > h5", elem);

                const name = $(".descriptionText > h5", elem)[0].lastChild.nodeValue;
                const country = codeToName[header[0].firstChild.attribs.src.split("/images/ui/flags/4x3/")[1].split('.')[0]]

                const description = Array.from($("li", elem)).map(li => li.firstChild.nodeValue).join('\n');
                const difficulty = getDifficulty($(".visible-xs-inline[src='/images/ui/bike-red.png']", elem).length);

                const containsWater = description.indexOf('water') != -1;

                const priceQuery = $(".price", elem).find('span')[1].firstChild.nodeValue.match(/\d+/);

                const price = priceQuery ? Number(priceQuery[0]) : -99999

                const length = Number($(".duration", elem)[0].lastChild.nodeValue.match(/\d+/));

                const image = `${SCRAPE_IMAGE_START_WITH}https://www.s-capetravel.eu/` + $(".imgthumb", elem)[0].attribs.src

                // number of month the trip is offerd as index for season
                const monthCount = Array.from($(".month", elem).map((x, y) => y.attribs.class)).filter(x => x.includes("2")).length;
                const recommendedSeason = getSeason(monthCount);

                return {
                    name, country, description, difficulty, containsWater, price, recommendedSeason, length, image
                }
            })
    })
}


function getDifficulty(index) {
    if (index <= 1) return 'Beginner';

    if (index === 2) return 'Intermediate';

    if (index === 3) return 'Advanced';

    if (index >= 4) return "Expert";
}


function getSeason(month) {

    if (month >= 3 && month <= 5) return 'Spring';
    if (month >= 6 && month <= 8) return 'Summer';
    if (month >= 9 && month <= 11) return 'Fall';

    // Months 12, 01, 02
    return 'Winter';
}

scrape();

module.exports = scrape;