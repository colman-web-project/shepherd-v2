import Comment from './comment.model';

export async function getAll() {
  return Comment.find();
}

export async function getOne({ params: { id } }) {
  return Comment.findById(id);
}

export async function create({ user, body: { comment } }) {
  comment.timestamp = Date.now();
  comment.user = user;
  return new Comment(comment).save();
}

export async function update({ params: { id }, body }) {
  return Comment.findByIdAndUpdate(id, { $set: body });
}

export async function remove({ params: { id } }) {
  return Comment.findByIdAndRemove(id);
}
