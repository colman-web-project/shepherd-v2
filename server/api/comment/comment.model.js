import { Schema, model } from "mongoose";

const schema = new Schema(
  {
    user: { type: Schema.Types.ObjectId, ref: "User" },
    route: { type: Schema.Types.ObjectId, ref: "Route" },
    content: String,
    timestamp: Date
  },
  { timestamps: true }
);

export default model("Comment", schema);
