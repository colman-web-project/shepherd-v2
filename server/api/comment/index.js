import { AsyncRouter } from "express-async-router";
import * as controller from "./comment.controller";
import { default as HyperLogLog } from "hyperloglog32";
import { isAuthenticated } from '../../auth/auth.service';
import Route from '../route/route.model'

const route = "comment";
const router = new AsyncRouter();

router.get("/", controller.getAll);
router.get("/:id", controller.getOne);
router.post("/", isAuthenticated, async (req,res) => {

const {body} = req;

console.log(body.comment.route);
const commentRoute = await Route.findById(body.comment.route);
console.log(commentRoute);

if (commentRoute) {
  controller.create(req);
} else {
  res.status(400).send();
}



});
router.delete("/:id", controller.remove);
router.put("/:id", controller.update);
router.get("/:id/comments", async req => getRelatedComments(req.param("id")));


router.get("/statistic/uniqueWords", async (req, res) => {
  // HLL structure with 12 bit indices
  const hll = HyperLogLog(12);

  let allComments = await controller.getAll();

  allComments.forEach(comment => {
    const commentWords = comment.content.match(/\S+/g); // Get array of only words in content
    commentWords.forEach(word => {
      hll.add(word);
    });
  });

  let distinctCount = hll.count();
  res.json(distinctCount);
});

export { route, router };
