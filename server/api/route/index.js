import { AsyncRouter } from 'express-async-router';
import * as controller from './route.controller';
import { multipartMiddleware } from '../../config/multipartMiddleware';

const route = 'route';
const router = new AsyncRouter();

router.get('/query', controller.query);
router.get('/by-difficulty', controller.routesByDifficulty);
router.get('/by-season', controller.routesBySeason);
router.get('/', controller.getAll);
router.get('/:id', async (req, res) => {
  

  const route = await controller.getOne(req);

  if (route) {
    res.json(route);

  } else {
    res.status(404).send();
  }

});
router.get('/:id/comments', async req => controller.getRelatedComments(req.param('id')));



// create
router.post('/', multipartMiddleware, async (req, res) => {
  // Get route object
  let route = JSON.parse(req.body.route);
  // Get uploaded image path
  if (req.files.file) {
    route.image = (req.files.file.path.split('\\').pop())
  }

  controller.create(route);
});

router.delete('/:id', controller.remove);

router.put('/:id', multipartMiddleware, async (req, res) => {
  // Get changed route object
  let changedRoute = JSON.parse(req.body.changedRoute);

  if (req.files.file) {
    changedRoute.image = (req.files.file.path.split('\\').pop())
  }

  controller.update(req.params.id, changedRoute)
});

export { route, router };
