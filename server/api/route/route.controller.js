import Route from './route.model';
import Comment from '../comment/comment.model';
import mongoose from 'mongoose';
import AhoCorasick from 'ahocorasick';
import _ from 'lodash';

export async function getAll() {
  return Route.find();
}

export async function getOne({ params: { id } }) {
  return Route.findById(id);
}

export async function create(route) {
  return new Route(route).save();
}

export async function update(id, changedRoute) {
  return Route.findByIdAndUpdate(id, { $set: changedRoute });
}

export async function remove({ params: { id } }) {
  return Route.findById(id, (err, route) => {
    if (err) {
      throw new Error(err);
    }

    route.remove();
  });
}

export async function getRelatedComments(id) {
  return await Comment.find({ route: mongoose.Types.ObjectId(id) }).populate("user");
}

export async function routesByDifficulty() {
  return (await Route.mapReduce({
    map: function () {
      emit(this.difficulty, 1);
    },
    reduce: function (k, values) {
      return values.length
    }
  })).results.map(x => ({ _id: x._id, count: x.value }));
}

function aggregateAlgResults(results) {
  return Object.entries(_.mapValues(_.fromPairs(results), _.max)).map(([key, value]) => ({
    start: Number(key) - value.length + 1,
    end: Number(key) + 1
  }));
}

export async function query({ query: { search } }) {
  const words = search.split(' ');

  const routes = await Route.find().exec();

  const alg = new AhoCorasick(words);

  const result = routes.map(({ _doc }) => {
    const matches = alg.search(_doc.description);

    return Object.assign({}, _doc, { matches: aggregateAlgResults(matches) })
  })
    .filter(({ matches }) => _.values(matches).length);

  return _.orderBy(result, [({ matches }) => _.values(matches).length], ['desc']);
}

export async function routesBySeason() {
  return await Route.aggregate([
    {
      $group: {
        _id: '$recommendedSeason',
        count: { $sum: 1 }
      }
    }
  ]);
}
