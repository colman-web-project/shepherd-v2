import { Schema, model } from "mongoose";
import Comment from '../comment/comment.model';

export const DifficultyLevel = Object.freeze({
  Beginner: "Beginner",
  Intermediate: "Intermediate",
  Advanced: "Advanced",
  Expert: "Expert"
});

export const Season = Object.freeze({
  Fall: "Fall",
  Winter: "Winter",
  Spring: "Spring",
  Summer: "Summer"
});

const schema = new Schema({
  site: { type: Schema.Types.ObjectId, ref: "Site" },
  name: String,
  difficulty: {
    type: String,
    enum: Object.values(DifficultyLevel)
  },
  length: Number,
  description: String,
  image: String,
  containsWater: Boolean,
  recommendedSeason: {
    type: String,
    enum: Object.values(Season)
  }
});

schema.post('remove', function (doc, next) {
  Comment.deleteMany({ 'route': doc._id }, {}, () => {
    next()
  });
});

export default model("Route", schema);
