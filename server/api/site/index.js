import { AsyncRouter } from "express-async-router";
import {
  getAll,
  getOne,
  getRelatedRoutes,
  add,
  deleteSite as remove,
  update,
  sitesByRegion
} from "./sites.controller";
import { multipartMiddleware } from "../../config/multipartMiddleware";

const route = "sites";

const router = new AsyncRouter();

router.param("id", async (req, res, param) => {
  req.site = await getOne(param);
});

router.get("/by-region", sitesByRegion);

router.get("/", getAll);

// create
router.post("/", multipartMiddleware, async (req, res) => {
  // Get site object
  let site = JSON.parse(req.body.site);
  // Get uploaded image path
  if (req.files.file) {
    site.image = req.files.file.path.split("\\").pop();
  }

  add(site);
});

router.get("/:id", async req => req.site);
// delete
router.delete("/:id", req => remove(req.site));

// update
router.put("/:id", multipartMiddleware, async (req, res) => {
  // Get changed site object
  let changedSite = JSON.parse(req.body.changedSite);

  if (req.files.file) {
    changedSite.image = req.files.file.path.split("\\").pop();
  }

  update(req.params.id, changedSite);
});

router.get("/:id/routes", async req => getRelatedRoutes(req.param("id")));

export { route, router };
