import { Schema, model } from "mongoose";

export const Region = Object.freeze({
  North: "North",
  South: "South",
  Central: "Central",
  Jerusalem: "Jerusalem"
});

const schema = new Schema({
  name: String,
  description: String,
  image: String,
  address: String,
  region: {
    type: String,
    enum: Object.values(Region)
  },
  // length: Number,
  // visitorsPerMonth: Number,
  // hasFoodArea: Boolean,
  // ticketPrice: Number
});

// TODO: GET COMMENTS AND ROUTES

export default model("Site", schema);
