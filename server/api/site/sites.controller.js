import Site from "./site.model";
import Route from "../route/route.model";
import mongoose from "mongoose";

export async function getAll() {
  return await Site.find();
}
//{ param: { id } }
export async function getOne(id) {
  return await Site.findById(id);
}

export async function deleteSite(site) {
  return await Site.findOneAndDelete({ _id: site._id });
}

export async function add(site) {
  return await new Site(site).save();
}

export async function update(id, changedSite) {
  return Site.findByIdAndUpdate(id, { $set: changedSite });
}

export async function getRelatedRoutes(id) {
  return await Route.find({ site: mongoose.Types.ObjectId(id) });
}

export async function sitesByRegion() {
  return await Site.aggregate([
    {
      $group: {
        _id: "$region",
        count: {
          $sum: 1
        }
      }
    }
  ]);
}
