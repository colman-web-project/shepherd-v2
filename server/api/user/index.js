import { AsyncRouter } from 'express-async-router';
import * as controller from './user.controller';
import { isAdmin, isAuthenticated } from '../../auth/auth.service';

const route = 'user';

const router = new AsyncRouter();

router.get('/', controller.getAll);
router.put('/me', isAuthenticated, controller.updateMe);
router.get('/:id', controller.getOne);
router.put('/:id/admin', isAuthenticated, isAdmin, controller.updateAdmin);
router.put('/:id', isAuthenticated, isAdmin, controller.update);
router.delete('/:id', isAuthenticated, isAdmin, controller.deleteOne);

export { route, router };
