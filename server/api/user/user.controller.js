import User from './user.model';
import Route from '../route/route.model';

export async function getAll() {
  return User.find();
}

export async function getOne({ params: { id } }) {
  return User.findById(id)
    .exec(err => {
      if (err) {
        throw new Error(err);
      }
    });
}

export async function deleteOne({ params: { id } }) {
  return User.findById(id, (err, user) => {
    if (err) {
      throw new Error(err);
    }

    user.remove();
  });
}

export async function updateAdmin({ params: { id }, body: { isAdmin } }) {
  return User.findByIdAndUpdate(id, { isAdmin });
}

export async function update({ params: { id }, body: { username, email } }) {
  return User.findByIdAndUpdate(id, { username, email });
}

export async function updateMe({ user, body: { oldPassword, newPassword } }, res, next) {
  try {

    if (newPassword) {
      await user.changePassword(oldPassword, newPassword);
    }

    res.json(user);

  } catch (e) {
    next(e);
  }
}
