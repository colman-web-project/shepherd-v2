import { Schema, model } from 'mongoose';
import passportPlugin from 'passport-local-mongoose';
import Comment from '../comment/comment.model';

const schema = new Schema({
  email: {
    type: String,
    required: true,
    match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  },
  isAdmin: {
    type: Boolean,
    required: true
  }
});

schema.post('remove', function (doc, next) {
  Comment.deleteMany({ 'user': doc._id }, {}, () => {
    next()
  });
});

schema.plugin(passportPlugin);

export default model('User', schema);
