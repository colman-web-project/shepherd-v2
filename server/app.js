import 'dotenv/config';
import express from 'express';
import helmet from 'helmet';
import { urlencoded, json } from 'body-parser';
import morgan from 'morgan';
import compression from 'compression';
import methodOverride from 'method-override';
import inProduction from 'in-production';
import passport from 'passport';
import cors from 'cors';
import errorHandler from 'express-error-log-handler';


import './config/mongoose';

import applyRoutes from './routes';

export default () => {
  const app = express();
  app.use(cors());
  app.use(helmet());
  app.use(urlencoded({ extended: true }));
  app.use(json());
  app.use(methodOverride());
  app.use(compression());
  app.use(passport.initialize());
  app.use(express.static('src/assets'));


  if (!inProduction) {
    app.use(morgan('dev'));
  }

  applyRoutes(app);

  app.use(errorHandler(({ statusCode, stack, message }) => {
    console.log('log')
    console.log(`${ statusCode } - ${ message } \n ${ stack }`)
  }));

  return app;
};
