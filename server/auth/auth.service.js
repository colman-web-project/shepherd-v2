import jwt from 'jsonwebtoken';
import expressJwt from 'express-jwt';
import pify from 'pify';
import createError from 'http-errors';

import User from '../api/user/user.model';

const validateToken = pify(expressJwt({ secret: process.env.JWT_SECRET }));

export async function isAuthenticated(req, res) {
  await validateToken(req, res);

  const user = await User.findById(req.user.id);

  if (!user) {
    throw new Error(createError(403));
  }

  req.user = user;
}

export async function isAdmin({user : { isAdmin }}) {
  if (!isAdmin){
    throw new error(createError(403))
  }
}

export function signToken({ id }) {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: '1d'
  });
}
