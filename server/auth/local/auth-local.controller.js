import passport from "passport";

import { signToken } from "../auth.service";
import User from "../../api/user/user.model";

export function register(req, res, next) {
  User.register(
    new User({ email: req.body.email, username: req.body.username, isAdmin: false }),
    req.body.password,
    (err, user) => {
      if (err) {
        return next(err);
      }

      req.login(user, { session: false }, err => {
        if (err) {
          return next(err);
        }

        return res.json({ token: signToken(user), user});
      });
    }
  );
}

export function login(req, res, next) {
  passport.authenticate(
    "local",
    { session: false },
    (err, user) => {

      if (err) {
        return next(err);
      }
      if (!user) {
        return res.status(404).send();
      }

      res.json({ token: signToken(user), user });
    }
  )(req, res, next);
}
