import { Router } from 'express';

import * as controller from './auth-local.controller';

const route = 'local';

const router = new Router();

router.post('/register', controller.register);
router.post('/login', controller.login);

export { route, router };
