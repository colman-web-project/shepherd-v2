import mongoose from 'mongoose';
import logger from '../logger';

const connection = mongoose
  .connect(
    process.env.MONGO_URI,
    {
      useNewUrlParser: true,
      useCreateIndex: true
    }
  )
  .then(logger.connectedToMongoDb)
  .catch(err => logger.error(`Failed to connect to MongoDb\n ${err}`));

mongoose.Promise = Promise;

export default connection;
