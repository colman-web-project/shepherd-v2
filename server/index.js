import port from './config/port';
import argv from './config/argv';
import logger from './logger';
import createApp from './app';
import io from 'socket.io';
import http from 'http';
import { socketHandler } from './sockets';


const app = createApp();
const httpHandler = http.createServer(app);

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const host = argv.host || process.env.HOST || null;

export let server;

export const started = new Promise(resolve => {
  const ioIntsance = io(httpHandler);
  socketHandler(ioIntsance);
  server = httpHandler.listen(port, host, err => {
    if (err) {
      logger.error(err.message);
    }

    logger.appStarted(port, host);

    resolve();
  });
});

export const close = () => {
  server.close();
};
