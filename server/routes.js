import { route as userRoute, router as userRouter } from './api/user';
import { route as authRoute, router as authRouter } from './auth';
import { route as siteRoute, router as siteRouter } from './api/site';
import { route as routeRoute, router as routeRouter } from './api/route';
import { route as commentRoute, router as commentRouter } from './api/comment';
import { seed } from './seed';

export default app => {
  app.use(`/api/${userRoute}`, userRouter);
  app.use(`/${authRoute}`, authRouter);
  app.use(`/api/${siteRoute}`, siteRouter);
  app.use(`/api/${routeRoute}`, routeRouter);
  app.use(`/api/${commentRoute}`, commentRouter);
  app.get('/seed', (req, res) => seed(req, res))
};
