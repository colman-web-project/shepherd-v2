import Site from './api/site/site.model';
import Route from './api/route/route.model';
import { sites, routes } from '../mock.data';
const scrape = require('../scrape');


export async function seed(req, res) {
    // const sitesSeed = sites.map((site) =>
    //     ({ ...site, _id: undefined })
    // );


    const sitesSeed = ["Belgium", "Croatia", "France", "Germany", "Greece", "Hungary", "Italy", "Netherlands", "Spain", "Portugal", "Slovakia", "Sweden", "Switzerland", "United Kingdom"].map(x => {

        return {
            name: x,
            address: x,
            description: "lorem ipsum",
            image: "lorem ipsum",
            region: "Central"
        }

    })


    let savedSites = await Site.create(sitesSeed);

    const byName = (name) => savedSites.find(x => x.name == name);

    const routes = await scrape()


    Route.create(routes.map(route => ({ ...route, site: byName(route.country) })));

    res.send(`Seeded with ${savedSites.length} sites and ${routes.length} routes!`)
}