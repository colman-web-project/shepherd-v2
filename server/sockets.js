/**
 * 
 * @param { SocketIO.Server} io 
 */
export const socketHandler = (io) => {
    let connectedUsers = 0;
    let currentNews = "There is a new route called 'Holland's Green Heart cycling'. It might interest you, go check it out!";

    io.on('connection', function (socket) {
        connectedUsers++;
        io.emit('stats', { connectedUsers });

        io.emit('news', { currentNews })
        console.log('Connected clients:', connectedUsers);

        socket.on("changeNews", message => {
            console.log(message);
            currentNews = message;

            io.emit('news', { currentNews })
        })

        socket.on('disconnect', function () {
            connectedUsers--;
            io.emit('stats', { connectedUsers });

            console.log('Connected clients:', connectedUsers);
        });
    });




}