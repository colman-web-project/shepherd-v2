import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  commentsUniqueWords$: Observable<number>;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.initCommentsDistinctCount();
  }

  initCommentsDistinctCount() {
    this.commentsUniqueWords$ = this.apiService.getCommentsDistinctCount();
  }

}
