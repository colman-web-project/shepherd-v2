import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { map, switchMap, startWith } from "rxjs/operators";
import { Observable, of, BehaviorSubject, combineLatest } from "rxjs";
import { RouteSearch } from "../models/types";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-advanced-search",
  templateUrl: "./advanced-search.component.html",
  styleUrls: ["./advanced-search.component.css"]
})
export class AdvancedSearchComponent implements OnInit {
  reload$ = new BehaviorSubject(null);
  matchedRoutes: RouteSearch[];
  isLoading = false;

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.params.subscribe(params => {
      const query = params["id"];

      this.reload$
        .pipe(
          switchMap(() => {
            this.isLoading = true;
            return this.api.searchQuery(query);
          })
        )
        .subscribe(routes => {
          this.matchedRoutes = routes;
          this.isLoading = false;
        });
    });
  }

  ngOnInit() {}

  handleDelete() {
    this.reload$.next(null);
  }
}
