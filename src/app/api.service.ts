import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Site, Route, User, GroupByQueryResult, RouteSearch } from './models/types';
import { AuthService } from './auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private baseUrl = 'http://localhost:3000/';

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  // -- Sites --
  getSites() {
    return this.http.get<Site[]>(`${this.baseUrl}api/sites`);
  }

  getSite(id: string) {
    return this.http.get<Site>(`${this.baseUrl}api/sites/${id}`);
  }

  updateSite(id: string, siteFormData: FormData) {
    return this.http.put<Site>(`${this.baseUrl}api/sites/${id}`, siteFormData);
  }

  deleteSite(id: string) {
    return this.http.delete(`${this.baseUrl}api/sites/${id}`);
  }

  getSiteRoutes(id: string) {
    return this.http.get<Route[]>(`${this.baseUrl}api/sites/${id}/routes`);
  }

  getSitesByRegion() {
    return this.http.get<GroupByQueryResult[]>(`${this.baseUrl}api/sites/by-region`);
  }

  createSite(siteFormData: FormData) {
    return this.http.post(`${this.baseUrl}api/sites`, siteFormData);
  }

  // -- Routes --

  getRoutes() {
    return this.http.get<Route[]>(`${this.baseUrl}api/route`);
  }

  getRoute(id: string) {
    return this.http.get<Route>(`${this.baseUrl}api/route/${id}`);
  }

  updateRoute(id: string, routeFormData: FormData) {
    return this.http.put<Route>(`${this.baseUrl}api/route/${id}`, routeFormData);
  }

  deleteRoute(id: string) {
    return this.http.delete(`${this.baseUrl}api/route/${id}`);
  }

  createRoute(routeFormData: FormData) {
    return this.http.post(`${this.baseUrl}api/route`, routeFormData);
  }

  getRouteComments(id: string) {
    return this.http.get<Route[]>(`${this.baseUrl}api/route/${id}/comments`);
  }

  getRoutesByDifficulty() {
    return this.http.get<GroupByQueryResult[]>(`${this.baseUrl}api/route/by-difficulty`);
  }

  getRoutesBySeason() {
    return this.http.get<GroupByQueryResult[]>(`${this.baseUrl}api/route/by-season`);
  }

  searchQuery(search: string) {
    const params = new HttpParams().set('search', search);
    return this.http.get<RouteSearch[]>(`${this.baseUrl}api/route/query`, {params});
  }

  // -- Comments --

  getCommentsDistinctCount() {
    return this.http.get<number>(`${this.baseUrl}api/comment/statistic/uniqueWords`);
  }

  createComment(commentFormData: any) {
    return this.http.post(`${this.baseUrl}api/comment`, commentFormData,
      {headers: new HttpHeaders({'Content-Type': 'application/json', ...this.getAuthHeader()})});
  }

  deleteComment(id: string) {
    return this.http.delete(`${this.baseUrl}api/comment/${id}`);
  }

  // -- Users --

  getUsers() {
    return this.http.get<User[]>(`${this.baseUrl}api/user`);
  }

  updateAdmin(id: string, isAdmin: boolean) {
    return this.http.put(`${this.baseUrl}api/user/${id}/admin`, {isAdmin},
      {headers: new HttpHeaders({'Content-Type': 'application/json', ...this.getAuthHeader()})});
  }

  updateUser(id: string, formData: object) {
    return this.http.put(`${this.baseUrl}api/user/${id}`, formData,
      {headers: new HttpHeaders({'Content-Type': 'application/json', ...this.getAuthHeader()})});
  }

  updatePassword(formData: object) {
    return this.http.put(`${this.baseUrl}api/user/me`, formData,
      {headers: new HttpHeaders({'Content-Type': 'application/json', ...this.getAuthHeader()})});
  }

  deleteUser(id: string) {
    return this.http.delete(`${this.baseUrl}api/user/${id}`,
      {headers: new HttpHeaders({...this.getAuthHeader()})});
  }

  // -- Auth --
  login(formData: object) {
    return this.http.post(`${this.baseUrl}auth/local/login`,
      formData,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})});
  }

  register(formData: object) {
    return this.http.post(`${this.baseUrl}auth/local/register`,
      formData,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})});
  }

  private getAuthHeader() {
    return {
      Authorization: `Bearer ${this.authService.getToken()}`
    };
  }
}
