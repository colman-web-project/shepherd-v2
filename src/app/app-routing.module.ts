import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { AuthGuard } from './auth/auth-guard';
import { RouteCreateUpdateComponent } from './route-create-update/route-create-update.component';
import { RouteViewComponent } from './route-view/route-view.component';
import { RoutesListComponent } from './routes-list/routes-list.component';
import { SiteCreateUpdateComponent } from './site-create-update/site-create-update.component';
import { SiteViewComponent } from './site-view/site-view.component';
import { SitesListComponent } from './sites-list/sites-list.component';
import { UsersListComponent } from './users-list/users-list.component';
import { HomePageComponent } from './home-page/home-page.component';
import { AuthModule } from './auth/auth.module';
import { NoAuthComponent } from './auth/no-auth/no-auth.component';
import { NotFoundComponent } from './404/404.component';
import { AdvancedSearchComponent } from './advanced-search/advanced-search.component';

const routes: Routes = [
  { path: 'home', component: HomePageComponent },
  { path: 'admin', component: AdminPageComponent, canActivate: [AuthGuard] },
  { path: 'sites', component: SitesListComponent },
  { path: 'sites/create', component: SiteCreateUpdateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'sites/:id', component: SiteViewComponent },
  { path: 'sites/update/:id', component: SiteCreateUpdateComponent, canActivate: [AuthGuard] },
  { path: 'routes', component: RoutesListComponent },
  { path: 'routes/create', component: RouteCreateUpdateComponent, canActivate: [AuthGuard] },
  { path: 'routes/:id', component: RouteViewComponent },
  { path: 'routes/update/:id', component: RouteCreateUpdateComponent, canActivate: [AuthGuard] },
  { path: 'noAuth', component: NoAuthComponent },
  { path: '404', component: NotFoundComponent },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'users',
    component: UsersListComponent,
    canActivate: [AuthGuard]
  },
  { path: 'search/:id', component: AdvancedSearchComponent },
  { path: '**', redirectTo: '/404' }];

@NgModule({
  imports: [RouterModule.forRoot(routes), AuthModule],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
