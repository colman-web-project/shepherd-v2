import { Component, ViewChild, ElementRef } from '@angular/core';
import { trigger, transition, query, style, animate } from '@angular/animations';
import { Observable} from 'rxjs';
import { ServerStatsService } from './server-stats.service';
import { MatDialog } from '@angular/material/dialog';
import { NewsModalComponent } from './news-modal/news-modal.component';
import { AuthDialogComponent } from './auth/auth-dialog.component';
import { ChangePasswordDialogComponent } from './auth/change-password/change-password-dialog.component';
import { AuthService } from './auth/auth.service';
import { Router } from '@angular/router';

const fadeAnimation = trigger('fade', [
  transition('* => *', [
    query(':enter', [
      style({ opacity: 0, transform: 'scale(0.5)' })
    ], { optional: true }),
    query(':leave', [
      style({ opacity: 1, transform: 'scale(1)' }),
      animate('0.2s', style({ opacity: 0, transform: 'scale(0.5)' }))
    ], { optional: true }),
    query(':enter', [
      style({ opacity: 0, transform: 'scale(0.5)' }),
      animate('0.2s', style({ opacity: 1, transform: 'scale(1)' }))
    ], { optional: true })
  ])
]);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [fadeAnimation]
})
export class AppComponent {
  title = 'web-project';
  users$: Observable<number>;

  @ViewChild('news') newsInput: ElementRef;

  news = '';
  currentUser$: Observable<any>;

  constructor(private serverStats: ServerStatsService,
              private dialog: MatDialog,
              private authService: AuthService,
              private router: Router) {

    this.users$ = this.serverStats.users$;
    this.currentUser$ = this.authService.currentUser$;
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/']);
  }

  onKeydown(e) {
    const query = e.target.value;
    this.router.navigate([query ? `/search/${query}` : "/routes"]);
  }

  openNews() {
    this.dialog.open(NewsModalComponent, {
      width: '350px'
    });
  }

  openAuthDialog() {
    this.dialog.open(AuthDialogComponent);
  }

  openChangePasswordDialog() {
    this.dialog.open(ChangePasswordDialogComponent);
  }
}
