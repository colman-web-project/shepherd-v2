import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphsModule } from './graphs/graphs.module';
import { MaterialImportsModule } from './material-imports/material-imports.module';
import { RoutesListComponent } from './routes-list/routes-list.component';
import { SharedModule } from './shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SiteViewComponent } from './site-view/site-view.component';
import { SitesListComponent } from './sites-list/sites-list.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { NewsDirective } from './news/news.directive';
import { NewsModalComponent } from './news-modal/news-modal.component';
import { RouteViewComponent } from './route-view/route-view.component';
import { RouteCreateUpdateComponent } from './route-create-update/route-create-update.component';
import { LoginComponent } from './auth/login/login.component';
import { SiteCreateUpdateComponent } from './site-create-update/site-create-update.component';
import { AuthDialogComponent } from './auth/auth-dialog.component';
import { MatSidenavModule, MatSlideToggleModule, MatTabsModule } from '@angular/material';
import { RegisterComponent } from './auth/register/register.component';
import { AuthModule } from './auth/auth.module';
import { UsersListComponent } from './users-list/users-list.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NoAuthComponent } from './auth/no-auth/no-auth.component';
import { NotFoundComponent } from './404/404.component';
import { AdvancedSearchComponent } from './advanced-search/advanced-search.component';
import { RouteDescriptionComponent } from './route-description/route-description.component';
import { ChangePasswordDialogComponent } from './auth/change-password/change-password-dialog.component';

const config: SocketIoConfig = { url: 'http://localhost:3000', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    SitesListComponent,
    SiteViewComponent,
    RoutesListComponent,
    AdminPageComponent,
    NewsDirective,
    NewsModalComponent,
    AuthDialogComponent,
    RouteViewComponent,
    RouteCreateUpdateComponent,
    LoginComponent,
    RegisterComponent,
    ChangePasswordDialogComponent,
    SiteCreateUpdateComponent,
    UsersListComponent,
    HomePageComponent,
    NoAuthComponent,
    AdvancedSearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SharedModule,
    MaterialImportsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SocketIoModule.forRoot(config),
    ReactiveFormsModule,
    FormsModule,
    GraphsModule,
    MatTabsModule,
    AuthModule,
    MatSidenavModule,
    MatSlideToggleModule
  ],
  entryComponents: [NewsModalComponent, AuthDialogComponent, ChangePasswordDialogComponent],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
