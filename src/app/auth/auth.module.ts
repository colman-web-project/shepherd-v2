import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IsLoggedDirective } from './is-logged.directive';
import { IsAdminDirective } from './is-admin.directive';
import { IsUserOwnerDirective } from './is-user-owner.directive';

@NgModule({
    declarations: [IsLoggedDirective, IsAdminDirective, IsUserOwnerDirective],
    imports: [CommonModule],
  exports: [IsLoggedDirective, IsAdminDirective, IsUserOwnerDirective],
    providers: [],
})
export class AuthModule { }
