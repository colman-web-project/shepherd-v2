import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/types';

@Injectable({providedIn: 'root'})
export class AuthService {
  private isLoggedInInternal$ = new BehaviorSubject<User>(null);

  get currentUser$() {
    return this.isLoggedInInternal$.asObservable();
  }

  constructor() {
    this.restoreCredentials();
  }

  restoreCredentials() {
    if (localStorage.getItem('user')) {
      const user = JSON.parse(localStorage.getItem('user'));
      this.isLoggedInInternal$.next(user);
    }
  }

  getUser() {
    if (localStorage.getItem('user')) {
      return JSON.parse(localStorage.getItem('user'));
    }
  }

  saveCredentials(token: any, user: any) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.isLoggedInInternal$.next(user);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.isLoggedInInternal$.next(null);
  }
}
