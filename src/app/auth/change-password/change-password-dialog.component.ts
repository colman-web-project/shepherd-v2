import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../api.service';
import { AuthService } from '../auth.service';
import { MatDialogRef } from '@angular/material/dialog';
import { NotifyService } from '../../notify-service/notify.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password-dialog.component.html'
})
export class ChangePasswordDialogComponent implements OnInit {
  userUpdateForm: FormGroup;
  currentUser$: Observable<any>;

  constructor(private formBuilder: FormBuilder,
              private api: ApiService,
              private dialogRef: MatDialogRef<ChangePasswordDialogComponent>,
              private authService: AuthService,
              private notify: NotifyService) {
    this.currentUser$ = this.authService.currentUser$;
  }

  ngOnInit() {
    this.initFormGroup();
  }

  initFormGroup() {
    this.userUpdateForm = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.userUpdateForm.invalid) {
      return;
    }

    return this.api.updatePassword({
      oldPassword: this.userUpdateForm.controls.oldPassword.value,
      newPassword: this.userUpdateForm.controls.newPassword.value
    }).subscribe(() => {
        this.dialogRef.close();
        this.notify.onOperationComplete('change succeeded!');
      },
      (error) => {
        this.notify.onOperationComplete('wrong credentials!');
      });
  }

  get f() {
    return this.userUpdateForm.controls;
  }
}
