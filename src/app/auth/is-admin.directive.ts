import { Directive, ContentChild, ViewContainerRef, TemplateRef, OnInit, OnDestroy, Input } from '@angular/core';
import { AuthService } from './auth.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[appIsAdmin]'
})
export class IsAdminDirective implements OnInit, OnDestroy {
  @Input('appIsAdmin') reverse: boolean;

  onDestroy$ = new Subject();

  constructor(private container: ViewContainerRef,
              private template: TemplateRef<any>,
              private authService: AuthService
  ) { }

  ngOnInit(): void {

    this.authService.currentUser$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(user => {
        if (!this.reverse && user && user.isAdmin || this.reverse && !user) {
          this.container.createEmbeddedView(this.template);
        } else {
          this.container.remove();
        }
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
}
