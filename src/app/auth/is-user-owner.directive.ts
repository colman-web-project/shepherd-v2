import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from './auth.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Directive({
  selector: '[appIsUserOwner]'
})
export class IsUserOwnerDirective implements OnInit, OnDestroy {
  @Input('appIsUserOwner') resource: { user: string };

  onDestroy$ = new Subject();

  constructor(private container: ViewContainerRef,
              private template: TemplateRef<any>,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.authService.currentUser$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(user => {
        if (this.resource.user && user && (this.resource.user === user._id || user.isAdmin)) {
          this.container.createEmbeddedView(this.template);
        } else {
          this.container.remove();
        }
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
}
