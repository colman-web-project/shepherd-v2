import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../api.service';
import { AuthService } from '../auth.service';
import { AuthDialogComponent } from '../auth-dialog.component';
import { MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;

  constructor(private api: ApiService,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private dialogRef: MatDialogRef<AuthDialogComponent>,
    private snackBar: MatSnackBar
  ) {
  }

  initFormGroup() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.initFormGroup();
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }

    return this.api.login({
      username: this.loginForm.controls.username.value,
      password: this.loginForm.controls.password.value
      // @ts-ignore
    }).subscribe(({ token, user }) => {
      this.authService.saveCredentials(token, user);
      this.dialogRef.close();
    }, (error) => {
      this.snackBar.open('wrong credentials!', 'close');
    });
  }

  get f() {
    return this.loginForm.controls;
  }

}
