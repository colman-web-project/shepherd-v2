import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../api.service';
import { AuthService } from '../auth.service';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthDialogComponent } from '../auth-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  constructor(private api: ApiService,
              private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<AuthDialogComponent>,
              private authService: AuthService,
              private snackBar: MatSnackBar) {
  }

  initFormGroup() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.initFormGroup();
  }

  onSubmit() {
    if (this.registerForm.invalid) {
      return;
    }

    return this.api.register({
      email: this.registerForm.controls.email.value,
      username: this.registerForm.controls.username.value,
      password: this.registerForm.controls.password.value
      // @ts-ignore
    }).subscribe(({token, user}) => {
        this.authService.saveCredentials(token, user);
        this.dialogRef.close();
      },
      (error) => {
        this.snackBar.open('wrong credentials!', 'close');
      });
  }

  get f() {
    return this.registerForm.controls;
  }

}
