import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutesBySeasonComponent } from './routes-by-season/routes-by-season.component';
import { RoutesByDifficultyComponent } from './routes-by-difficulty/routes-by-difficulty.component';

@NgModule({
  declarations: [RoutesBySeasonComponent, RoutesByDifficultyComponent],
  exports: [RoutesBySeasonComponent, RoutesByDifficultyComponent],
  imports: [
    CommonModule
  ]
})
export class GraphsModule { }
