import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutesByDifficultyComponent } from './routes-by-difficulty.component';

describe('RoutesByDifficultyComponent', () => {
  let component: RoutesByDifficultyComponent;
  let fixture: ComponentFixture<RoutesByDifficultyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutesByDifficultyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutesByDifficultyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
