import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import * as d3 from 'd3';
import { GroupByQueryResult } from 'src/app/models/types';


@Component({
  selector: 'app-routes-by-difficulty',
  template: '<svg id="routeByDifficultyPie" class="pie" viewBox="0 0 800 200" #grpah />',
  styleUrls: ['./routes-by-difficulty.component.css']
})
export class RoutesByDifficultyComponent implements AfterViewInit {

  @ViewChild('grpah') grpah: ElementRef;

  constructor(private api: ApiService) { }

  ngAfterViewInit() {
    this.api
      .getRoutesByDifficulty()
      .toPromise()
      .then(result => this.drawRoutePerDifficulty(result))
  }

  drawRoutePerDifficulty(data: GroupByQueryResult[]) {
    // Graph sizes
    var margin = {
      top: 15,
      right: 0,
      bottom: 30,
      left: 80
    };

    var width = 800 - margin.left - margin.right;
    var height = 200 - margin.top - margin.bottom;
    var maxSiteCount = d3.max(data, (entry) => entry.count);
    var color = d3.scaleOrdinal(d3.schemeCategory10);

    // Scales for axes - x for seasons, y for route count
    var x = d3.scaleBand()
      .rangeRound([0, width]) //, .1
      .padding(0.5)
      .domain(data.map((d) => d._id));

    var y = d3
      .scaleLinear()
      .rangeRound([height, 0])
      .domain([0, maxSiteCount]);

    // Define the axes
    var xAxis = d3.axisBottom(x);

    var yAxis = d3.axisLeft(y)
      .ticks(maxSiteCount);

    // Prepare the working area
    var svg = d3
      .select(this.grpah.nativeElement)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Append the axes
    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis);

    // Textual description
    svg.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Route Count");

    // Append the data
    svg.selectAll()
      .data(data)
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("fill", (d) => color(d._id))
      .attr("x", (d) => x(d._id))
      .attr("width", x.bandwidth())
      .attr("y", height)
      .attr("height", 0)
      .transition()
      .duration(600)
      .attr("y", (d) => y(d.count))
      .attr("height", (d) => height - y(d.count));
  }
}
