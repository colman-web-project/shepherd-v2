import { Component } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable, BehaviorSubject } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { Site } from "../models/types";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent {
  reload$ = new BehaviorSubject(null);
  sites$: Observable<Site[]>;
  addresses$: any;

  constructor(private api: ApiService) { 
    this.sites$ = this.reload$.pipe(switchMap(() => this.api.getSites()));

    this.addresses$ = this.sites$.pipe(
      map(sites => sites.map(site => site.address))
    );
  }
}
