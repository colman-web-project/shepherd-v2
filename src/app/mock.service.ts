import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Site, Route } from './models/types';
import { sites, routes } from 'mock.data.js';


@Injectable({
  providedIn: "root"
})
export class MockService {
  constructor() { }

  // TODO: get from server using mongo query
  getSites(): Observable<Site[]> {
    return of(sites);
  }

  // TODO: get from server using mongo query
  getSite(id: string): Observable<Site> {
    return of(sites.find(site => site._id === id));
  }

  // TODO: get from server using mongo query
  getRoutes(): Observable<Route[]> {
    return of(routes);
  }

  // TODO: get from server using mongo query
  getSiteRoutes(siteId: string) {
    return of(routes.filter(route => route.site === siteId))
  }
}
