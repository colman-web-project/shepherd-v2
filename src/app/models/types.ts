export interface Site {
  _id: string;
  name: string;
  difficulty: string;
  length: number;
  address: string;
  description: string;
  image: string;
  visitorsPerMonth: number;
  hasFoodArea: boolean;
  ticketPrice: number;
  region: string;
}

export interface Route {
  _id: string;
  site: string;
  name: string;
  difficulty: string;
  length: number;
  description: string;
  image: string;
  containsWater: boolean;
  recommendedSeason: string;
}

export interface User {
  _id: string;
  username: string;
  email: string;
  isAdmin: boolean;
}

export interface GroupByQueryResult {
  _id: string;
  count: number;
}

export interface SocketStatsMessage {
  connectedUsers: number;
  currentNews: string;
}

interface Match {
  start: number;
  end: number;
}

export interface RouteSearch extends Route {
  matches: Match[];
}
