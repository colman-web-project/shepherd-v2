import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-news-modal',
  templateUrl: './news-modal.component.html',
  styleUrls: ['./news-modal.component.css']
})
export class NewsModalComponent {

  constructor(
    public dialogRef: MatDialogRef<NewsModalComponent>,
    /*@Inject(MAT_DIALOG_DATA) public data: DialogData*/) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
