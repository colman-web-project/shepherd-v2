import { Component, OnInit, EventEmitter, Input, Output, SimpleChanges, Directive } from '@angular/core';
import { ServerStatsService } from '../server-stats.service';
import { Observable, Subscription } from 'rxjs';
import { startWith } from 'rxjs/operators';

@Directive({
  selector: '[app-news]',

})
export class NewsDirective implements OnInit {

  @Input() news: string;
  @Output() newsChange = new EventEmitter();
  latest: string;
  private sub: Subscription;

  constructor(private appStats: ServerStatsService) {

  }

  ngOnInit(): void {

    this.sub = this.appStats.news$.subscribe(x => {
      this.latest = x;
      this.newsChange.emit(x);
    })
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.news.previousValue
      && changes.news.currentValue != ""
      && this.news != changes.news.previousValue
      && this.news != this.latest
      && this.news == changes.news.currentValue) {
      this.latest = this.news;
      this.appStats.updateNews(changes.news.currentValue);
    }
  }
}
