import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  constructor(private snackbar: MatSnackBar) {
  }

  onOperationComplete(message: string, action: string = null, options: object = {duration: 2000}) {
    this.snackbar.open(message, action, options);
  }
}
