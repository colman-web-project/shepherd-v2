import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteCreateUpdateComponent } from './route-create-update.component';

describe('RouteCreateUpdateComponent', () => {
  let component: RouteCreateUpdateComponent;
  let fixture: ComponentFixture<RouteCreateUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteCreateUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteCreateUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
