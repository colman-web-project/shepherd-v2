import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Site, Route } from '../models/types';
import { Observable } from 'rxjs';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { UploadsService } from '../uploads.service';

const SCRAPE_IMAGE_START_WITH = "scrape_";

@Component({
  selector: 'app-route-create-update',
  templateUrl: './route-create-update.component.html',
  styleUrls: ['./route-create-update.component.css']
})
export class RouteCreateUpdateComponent implements OnInit {

  routeForm: FormGroup;
  sites$: Observable<Site[]>;
  image: File;
  routeToUpdate: Route;
  routeImageSrc: string;

  constructor(private api: ApiService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uploadsService: UploadsService) { }

  async ngOnInit() {
    // Get all sites
    this.sites$ = this.api.getSites();

    // Get route id from params
    const routeId = this.route.snapshot.params["id"];

    // Load route image default src
    this.getRouteImageUrl();

    // If update mode, init route object to update
    if (routeId) {
      await this.initRouteToUpdate(routeId);
    }

    // Init group of the forms
    this.initFormGroup();
  }

  async initRouteToUpdate(routeId: string) {

    try {
      this.routeToUpdate = await this.api.getRoute(routeId).toPromise();
      // If route has image, load it
      this.getRouteImageUrl();
    } catch (error) {
      this.router.navigate(['/404']);
    }
  }

  getRouteImageUrl() {
    const image = this.routeToUpdate && this.routeToUpdate.image;
    this.routeImageSrc = image && image.startsWith(SCRAPE_IMAGE_START_WITH)
      ? image.slice(SCRAPE_IMAGE_START_WITH.length, image.length)
      : this.uploadsService.getUploadedImageUrl(image);
  }

  initFormGroup() {
    this.routeForm = this.formBuilder.group({
      site: [this.routeToUpdate ? this.routeToUpdate.site : '', Validators.required],
      name: [this.routeToUpdate ? this.routeToUpdate.name : '', Validators.required],
      description: [this.routeToUpdate ? this.routeToUpdate.description : ''],
      length: [this.routeToUpdate ? this.routeToUpdate.length : '', [Validators.required, Validators.min(0), Validators.max(30)]],
      recommendedSeason: [this.routeToUpdate ? this.routeToUpdate.recommendedSeason : '', Validators.required],
      containsWater: [this.routeToUpdate ? this.routeToUpdate.containsWater : false],
      difficulty: [this.routeToUpdate ? this.routeToUpdate.difficulty : '', Validators.required]
    })
  }

  onSubmit() {
    // stop here if form is invalid
    if (this.routeForm.invalid) {
      return;
    }

    const formData = new FormData();

    if (this.image) {
      formData.append("file", this.image, this.image.name);
    }

    // Update mode
    if (this.routeToUpdate) {
      formData.append("changedRoute", JSON.stringify((this.routeForm.value)));
      this.api.updateRoute(this.routeToUpdate._id, formData).subscribe(this.onUpdate.bind(this));
    }
    // Create mode
    else {
      formData.append("route", JSON.stringify((this.routeForm.value)));
      this.api.createRoute(formData).subscribe(this.onCreate.bind(this));

    }
  }

  onCreate(routeId: any) {
    this.router.navigate([`/routes`])
  }

  onUpdate() {
    this.router.navigate([`/routes/${this.routeToUpdate._id}`])
  }

  // convenience getter for easy access to form fields
  get f() { return this.routeForm.controls; }

}
