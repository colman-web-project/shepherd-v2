import { Component, OnInit, Input } from '@angular/core';
import { Route } from '../models/types';

@Component({
  selector: 'app-route-description',
  templateUrl: './route-description.component.html',
  styleUrls: ['./route-description.component.css']
})
export class RouteDescriptionComponent implements OnInit {
  @Input() route: Route;

  constructor() { }

  ngOnInit() {
  }
}
