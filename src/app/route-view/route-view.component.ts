import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Route } from '../models/types';
import { Observable, BehaviorSubject, of } from 'rxjs';
import {
  FormGroup,
  Validators,
  FormBuilder
} from '@angular/forms';
import { switchMap, catchError } from 'rxjs/operators';
import { NotifyService } from '../notify-service/notify.service';

@Component({
  selector: 'app-route-view',
  templateUrl: './route-view.component.html',
  styleUrls: ['./route-view.component.css']
})
export class RouteViewComponent implements OnInit {
  commentForm: FormGroup;
  route$: Observable<Route>;
  routeComments$: Observable<Route[]>;

  reloadComments$ = new BehaviorSubject(null);

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private notify: NotifyService
  ) {
  }

  ngOnInit() {
    const routeId = this.route.snapshot.params.id;
    this.route$ = this.api.getRoute(routeId)
      .pipe(catchError(() => {
        this.router.navigate(['/404']);
        return of({} as any);
      }));
    this.routeComments$ = this.reloadComments$.pipe(switchMap(() => this.api.getRouteComments(routeId)));
    this.commentForm = this.formBuilder.group({
      route: [routeId],
      content: ['', Validators.required]
    });
  }

  handleDelete() {
    this.router.navigate(['/routes']);
    this.notify.onOperationComplete('route deleted');
  }

  onSubmit() {
    if (this.commentForm.invalid) {
      return;
    }

    this.api.createComment({comment: this.commentForm.value}).subscribe(() => {
      this.reloadComments$.next(true);
      this.commentForm.controls.content.reset();
    }, (error) => {
      this.notify.onOperationComplete('שגיאה בהוספת תגובה, יתכן והמסלול נמחק. נסה שנית', 'אוקיי', {duration: 5000});
    });
  }

  handleOnCommentDelete(comment) {
    this.api.deleteComment(comment._id).subscribe(() => {
      this.reloadComments$.next(true);
      this.notify.onOperationComplete('comment deleted');
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.commentForm.controls;
  }
}
