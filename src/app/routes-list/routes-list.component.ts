import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { map, switchMap, startWith } from "rxjs/operators";
import { Observable, of, BehaviorSubject, combineLatest } from "rxjs";
import { Route } from '../models/types';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: "app-routes-list",
  templateUrl: "./routes-list.component.html",
  styleUrls: ["./routes-list.component.css"]
})
export class RoutesListComponent implements OnInit {
  reload$ = new BehaviorSubject(null);
  routes$: Observable<Route[]>;
  searchName: string = "";
  searchSeason: string = "All";
  filteredRoutes: Observable<Route[]>;
  myForm: FormGroup;

  constructor(private api: ApiService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.routes$ = this.reload$.pipe(switchMap(() => this.api.getRoutes()));
    this.myForm = this.formBuilder.group({
      searchSeason: 'All',
      difficulty: 'All',
      searchName: ''
    });

    this.filteredRoutes = combineLatest(this.routes$, this.myForm.valueChanges.pipe(startWith(null)))
      .pipe(
        map(
          ([routes, form]) => !form ? routes : this.filter(routes, form)
        ))
  }

  clear() {
    this.myForm.reset({
      searchRegion: 'All',
      difficulty: 'All',
      searchName: ''
    })
  }

  filter(routes, values) {
    return routes.filter(route =>
      (values.searchSeason === "All" || route.recommendedSeason === values.searchSeason) && 
      (!values.searchName || route.name.toUpperCase().includes(values.searchName.toUpperCase())) && 
      (values.difficulty === "All" || route.difficulty === values.difficulty)
    )
  }

  handleDelete() {
    this.reload$.next(null);
  }
}
