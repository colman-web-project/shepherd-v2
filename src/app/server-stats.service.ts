import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { map } from 'rxjs/operators';
import { SocketStatsMessage } from './models/types';
import { Observable, ReplaySubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ServerStatsService {
  news$ = new ReplaySubject<string>(1);
  users$: Observable<number>;

  constructor(private socket: Socket) {

    this.users$ = this.socket
      .fromEvent("stats")
      .pipe(
        map((message: SocketStatsMessage) => message.connectedUsers)
      )

    this.socket
      .fromEvent("news")
      .pipe(
        map((message: SocketStatsMessage) => message.currentNews)
      ).subscribe(this.news$);

  }

  updateNews(msg: string) {
    this.socket.emit("changeNews", msg);
  }

}

