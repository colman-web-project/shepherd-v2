import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowseImageComponent } from './browse-image.component';

describe('BrowseImageComponent', () => {
  let component: BrowseImageComponent;
  let fixture: ComponentFixture<BrowseImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrowseImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowseImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
