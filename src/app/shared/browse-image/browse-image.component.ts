import { Component, OnInit, Output , EventEmitter, Input} from '@angular/core';
import { Stringifiable } from 'd3';

@Component({
  selector: 'app-browse-image',
  templateUrl: './browse-image.component.html',
  styleUrls: ['./browse-image.component.css']
})
export class BrowseImageComponent implements OnInit {

  @Input() imageSrc: string;
  @Output() onImageSelected = new EventEmitter();

  image: File;

  constructor() { }

  ngOnInit() {
  }

  selectFiles = (event) => { //image upload handler

    let files: FileList = event.target.files;
    if (files && files[0]) {
      const file = files[0];;
      if (file.name.match(/\.(jpg|jpeg|png|gif)$/)) { //image validity check
        this.image = files[0];

        var reader = new FileReader();
        reader.readAsDataURL(file); // read file as data url

        reader.onload = (event) => { // called once readAsDataURL is completed
          this.imageSrc = (event.target as any).result;
        }
      }

      this.onImageSelected.emit(this.image);
    }

  }

}
