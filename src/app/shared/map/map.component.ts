import { Component, Input, ViewChild, SimpleChanges } from '@angular/core';
import { BingMapsLoader } from '../map-loader.service';

@Component({
  selector: 'app-map',
  template: `
    <div #myMap style='width: 100%; height: 500px;'></div> 
`
})
export class MapComponent {
  @ViewChild('myMap') myMap;
  @Input() addresses: string[]
  // @ts-ignore
  map: Microsoft.Maps.Map;

  constructor() {  }

  ngAfterViewInit(): void {
    BingMapsLoader
      .load()
      .then(() => this.loadMap());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.map && changes.addresses.currentValue) {
      this.addLocations();
    }
  }

  loadMap() {  // after the view completes initialization, create the map
    //@ts-ignore
    this.map = new Microsoft.Maps.Map(this.myMap.nativeElement, {
      zoom: 3,
      // @ts-ignore
      center: new Microsoft.Maps.Location(54.5260, 5.2551),
      credentials: 'AlGXNbxRulVaMcB6mvrICM4NQT186_x3Nyw4n0GFiNLx3LcZzQ4xi5zzWHRHL3rJ'
    });
    //@ts-ignore
    let layer = new Microsoft.Maps.Layer();

    this.map.layers.insert(layer);

    if (this.addresses) {
      this.addLocations();
    }
  }

  private addLocations() {
    this.map.entities.clear();
    //@ts-ignore
    let addressSearchCallback = response => this.map.entities.push(new Microsoft.Maps.Pushpin(response.results[0].location));
    //@ts-ignore
    Microsoft.Maps.loadModule('Microsoft.Maps.Search', () => {
      //@ts-ignore
      let searchManager = new Microsoft.Maps.Search.SearchManager(this.map);
      this.addresses.forEach(address => searchManager.geocode({
        bounds: this.map.getBounds(),
        where: address,
        callback: addressSearchCallback
      }));
    });
  }
}
