import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Route } from 'src/app/models/types';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/api.service';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';
import {  UploadsService } from 'src/app/uploads.service';
const SCRAPE_IMAGE_START_WITH = 'scrape_';

@Component({
  selector: 'app-route',
  templateUrl: './route.component.html',
  styleUrls: ['./route.component.css']
})
export class RouteComponent implements OnInit {
  routeImageUrl: string;

  @Input() route: Route;
  @Input() showMoreInfo: boolean;
  @Input() showDelete: boolean;
  @Input() showEdit: boolean;

  @Output() onDelete = new EventEmitter();

  constructor(
    public dialog: MatDialog,
    private api: ApiService,
    private uploadsService: UploadsService
  ) {}

  ngOnInit() {
    this.getRouteImageUrl();
  }

  handleOnDelete() {
    const dialogRef = this.dialog.open(DeleteModalComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.api.deleteRoute(this.route._id).subscribe(this.onDelete);
      }
    });
  }

  getRouteImageUrl() {
    this.routeImageUrl = this.route.image.startsWith(SCRAPE_IMAGE_START_WITH)
      ? this.route.image.slice(SCRAPE_IMAGE_START_WITH.length, this.route.image.length)
      : this.uploadsService.getUploadedImageUrl(this.route.image);
  }
}
