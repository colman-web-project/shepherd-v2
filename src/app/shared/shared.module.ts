import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteComponent } from './site/site.component';
import { MaterialImportsModule } from '../material-imports/material-imports.module';
import { ListItemComponent } from './list-item/list-item.component';
import { RouterModule } from '@angular/router';
import { RouteComponent } from './route/route.component';
import { MapComponent } from './map/map.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { BrowseImageComponent } from './browse-image/browse-image.component';
import { IsLoggedDirective } from '../auth/is-logged.directive';
import { AuthModule } from '../auth/auth.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouteDescriptionComponent } from '../route-description/route-description.component';

@NgModule({
  declarations: [SiteComponent,
    ListItemComponent,
    RouteComponent,
    MapComponent,
    DeleteModalComponent,
    BrowseImageComponent,
    RouteDescriptionComponent
  ],
  exports: [SiteComponent,
    ListItemComponent,
    RouteComponent,
    MapComponent,
    RouterModule,
    MaterialImportsModule,
    DeleteModalComponent,
    BrowseImageComponent,
    MatProgressSpinnerModule
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialImportsModule,
    AuthModule,
    MatProgressSpinnerModule,
  ],
  entryComponents: [DeleteModalComponent]

})
export class SharedModule {
}
