import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Site } from 'src/app/models/types';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/api.service';
import { UploadsService } from 'src/app/uploads.service';

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.css']
})
export class SiteComponent implements OnInit {

  @Input() site: Site;
  @Input() showMoreInfo: boolean
  @Input() showDelete: boolean
  @Input() showEdit: boolean


  @Output() onDelete = new EventEmitter();

  siteImageUrl: string;


  constructor(public dialog: MatDialog, private api: ApiService,
    private uploadsService: UploadsService) { }

  ngOnInit() {
    this.getSiteImageUrl();
  }

  handleOnDelete(): void {

    const dialogRef = this.dialog.open(DeleteModalComponent, {
      width: '250px'
    });


    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.api.deleteSite(this.site._id).subscribe(this.onDelete);
      }
    });
  }

  getSiteImageUrl() {
    this.siteImageUrl = this.site.image.includes("http") ? this.site.image : this.uploadsService.getUploadedImageUrl(this.site.image);
  }

}
