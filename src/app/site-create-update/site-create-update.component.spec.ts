import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteCreateUpdateComponent } from './site-create-update.component';

describe('SiteCreateUpdateComponent', () => {
  let component: SiteCreateUpdateComponent;
  let fixture: ComponentFixture<SiteCreateUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteCreateUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteCreateUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
