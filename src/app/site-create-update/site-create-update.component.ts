import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Site } from '../models/types';
import { UploadsService } from '../uploads.service';

@Component({
  selector: 'app-site-create-update',
  templateUrl: './site-create-update.component.html',
  styleUrls: ['./site-create-update.component.css']
})
export class SiteCreateUpdateComponent implements OnInit {

  siteForm: FormGroup;
  image: File;
  siteToUpdate: Site;
  siteImageSrc: string;

  constructor(private api: ApiService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uploadsService: UploadsService) { }

  async ngOnInit() {
    // Get site id from params
    const siteId = this.route.snapshot.params["id"];

    // Load site image default src
    this.siteImageSrc = this.uploadsService.getUploadedImageUrl(null);

    // If update mode, init site object to update
    if (siteId) {
      await this.initSiteToUpdate(siteId);
    }

    // Init group of the forms
    this.initFormGroup();
  }

  async initSiteToUpdate(siteId: string) {
    try {
      this.siteToUpdate = await this.api.getSite(siteId).toPromise();

      // If site has image, load it
      if (this.siteToUpdate.image) {
        this.siteImageSrc = this.uploadsService.getUploadedImageUrl(this.siteToUpdate.image);
      }

    } catch (e) {
      this.router.navigate(['/404']);
    }
  }

  initFormGroup() {
    this.siteForm = this.formBuilder.group({
      name: [this.siteToUpdate ? this.siteToUpdate.name : '', Validators.required],
      description: [this.siteToUpdate ? this.siteToUpdate.description : ''],
      // visitorsPerMonth: [this.siteToUpdate ? this.siteToUpdate.visitorsPerMonth : '', [Validators.required, Validators.min(0)]],
      region: [this.siteToUpdate ? this.siteToUpdate.region : '', Validators.required],
      // ticketPrice: ['', [Validators.required, Validators.min(0)]],
      address: [this.siteToUpdate ? this.siteToUpdate.address : '', Validators.required],
      // hasFoodArea: [this.siteToUpdate ? this.siteToUpdate.visitorsPerMonth : false, Validators.required]
    })
  }


  onSubmit() {
    // stop here if form is invalid
    if (this.siteForm.invalid) {
      return;
    }

    const formData = new FormData();

    if (this.image) {
      formData.append("file", this.image, this.image.name);
    }

    // Update mode
    if (this.siteToUpdate) {
      formData.append("changedSite", JSON.stringify((this.siteForm.value)));
      this.api.updateSite(this.siteToUpdate._id, formData).subscribe(this.onUpdate.bind(this));
    }
    // Create mode
    else {
      formData.append("site", JSON.stringify((this.siteForm.value)));
      this.api.createSite(formData).subscribe(this.onCreate.bind(this));

    }
  }

  onCreate(routeId: any) {
    this.router.navigate([`/routes`])
  }

  onUpdate() {
    this.router.navigate([`/sites/${this.siteToUpdate._id}`])
  }

  // convenience getter for easy access to form fields
  get f() { return this.siteForm.controls; }
}
