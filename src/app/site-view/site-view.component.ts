import { Component, OnInit } from '@angular/core';
import { MockService } from '../mock.service';
import { Site, Route } from '../models/types';
import { Observable } from 'rxjs';
import { BingMapsLoader } from '../shared/map-loader.service';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-site-view',
  templateUrl: './site-view.component.html',
  styleUrls: ['./site-view.component.css']
})
export class SiteViewComponent implements OnInit {
  site$: Observable<Site>;
  siteRoutes$: Observable<Route[]>;


  constructor(private api: ApiService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit() {
    const siteId = this.route.snapshot.params["id"];
    this.siteRoutes$ = this.api.getSiteRoutes(siteId);
    this.site$ = this.api.getSite(siteId);
  }

  handleDelete() {
    this.router.navigate(['/sites'])
  }

}
