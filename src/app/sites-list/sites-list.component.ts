import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { Observable, BehaviorSubject, combineLatest } from "rxjs";
import { map, switchMap, startWith } from "rxjs/operators";
import { Site } from "../models/types";
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: "app-sites-list",
  templateUrl: "./sites-list.component.html",
  styleUrls: ["./sites-list.component.css"]
})
export class SitesListComponent implements OnInit {
  reload$ = new BehaviorSubject(null);
  sites$: Observable<Site[]>;
  addresses$: any;
  filteredSites$: Observable<Site[]>;
  myForm: FormGroup;

  constructor(private api: ApiService, private _fb: FormBuilder) { }




  ngOnInit() {
    this.myForm = this._fb.group({
      searchRegion: 'All',
      searchName: ''
    });

    this.sites$ = this.api.getSites();

    // this.sites$ = this.reload$.pipe(switchMap(() => this.api.getSites()));

    this.filteredSites$ = combineLatest(this.sites$, this.myForm.valueChanges.pipe(startWith(null)))
      .pipe(
        map(
          ([sites, form]) => !form ? sites : this.filterSites(sites, form)
        ))

    this.addresses$ = this.filteredSites$.pipe(
      map(sites => sites.map(site => site.address))
    );
  }

  private filterSites(sites: Site[], values: any): Site[] {
    return sites.filter(site => (values.searchRegion === "All" ||
      site.region === values.searchRegion) &&
      (!values.searchName || site.name.includes(values.searchName)));
  }

  clear() {
    this.myForm.reset({
      searchRegion: 'All',
      searchName: ''
    })

  }

  // filter() {
  //   this.filteredSites = this.sites$.pipe(
  //     map(sites =>
  //       sites.filter(
  //         site =>
  //           (this.searchRegion === "All" ||
  //             site.region === this.searchRegion) &&
  //           (!this.searchName || site.name.includes(this.searchName))
  //       )
  //     )
  //   );
  // }

  handleDelete() {
    this.reload$.next(null);
  }
}
