import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class UploadsService {

  constructor() { }

  getUploadedImageUrl(imageName: string) {

    // Default image
    if(!imageName) {
      imageName = "no-image-box.png";
    }

    return `http://localhost:3000/uploads/${imageName}`
  }
}
