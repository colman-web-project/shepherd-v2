import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { switchMap, startWith, map } from 'rxjs/operators';
import { BehaviorSubject, Observable, combineLatest } from 'rxjs';
import { User } from '../models/types';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DeleteModalComponent } from '../shared/delete-modal/delete-modal.component';
import { NotifyService } from '../notify-service/notify.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  userUpdateForm: FormGroup;
  users$: Observable<User[]>;
  reload$ = new BehaviorSubject(null);
  selectedUser;
  currentUser$: Observable<any>;
  filteredUsers$: Observable<User[]>;
  myForm: FormGroup;

  constructor(private api: ApiService,
              private snackbar: MatSnackBar,
              private dialog: MatDialog,
              private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private notify: NotifyService) {
  }

  ngOnInit() {
    this.users$ = this.reload$.pipe(switchMap(() => this.api.getUsers()));
    this.currentUser$ = this.authService.currentUser$;
    this.myForm = this.formBuilder.group({
      isAdmin: 'All',
      email: '',
      username: ''
    });

    this.filteredUsers$ = combineLatest(this.users$, this.myForm.valueChanges.pipe(startWith(null)))
    .pipe(
      map(
        ([users, form]) => !form ? users : this.filter(users, form)
      ));
  }

  filter(users, values) {
    return users.filter(user =>
      (values.isAdmin === "All" || (user.isAdmin && values.isAdmin === "Yes") || (!user.isAdmin && values.isAdmin === "No")) &&
      (!values.username || user.username.toUpperCase().includes(values.username.toUpperCase())) &&
      (!values.email || user.email.toUpperCase().includes(values.email.toUpperCase()))
    );
  }

  editUser(user) {
    this.selectedUser = user;

    this.initFormGroup();
  }

  updateAdmin() {
    this.api.updateAdmin(this.selectedUser._id, this.selectedUser.isAdmin)
      .subscribe(res => this.notify.onOperationComplete('update succeeded!'));
  }

  deleteUser(user) {
    const dialogRef = this.dialog.open(DeleteModalComponent, {width: '250px'});

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      this.api.deleteUser(user._id)
        .subscribe(() => {
          this.reload$.next(null);
          this.notify.onOperationComplete('delete succeeded!');

          if (this.authService.getUser()._id === user._id) {
            this.authService.logout();
            this.router.navigate([`/home`]);
          }
        });
    });
  }

  initFormGroup() {
    if (!this.selectedUser) {
      throw new Error('user not selected');
    }

    this.userUpdateForm = this.formBuilder.group({
      email: [this.selectedUser.email, [Validators.required, Validators.email]],
      username: [this.selectedUser.username, Validators.required]
    });
  }

  onSubmit() {
    if (this.userUpdateForm.invalid) {
      throw new Error('invalid values provided');
    }

    if (!this.userUpdateForm.dirty) {
      return;
    }

    this.api.updateUser(this.selectedUser._id, this.userUpdateForm.value)
      .subscribe(() => {
          this.reload$.next(null);
          this.initFormGroup();
          this.notify.onOperationComplete('update succeeded!');
        },
        (err) => {
          this.notify.onOperationComplete('invalid values provided');
        });
  }

  get f() {
    return this.userUpdateForm.controls;
  }

  isSameUser(user: User, otherUser: User) {
    return user._id === otherUser._id;
  }

}
